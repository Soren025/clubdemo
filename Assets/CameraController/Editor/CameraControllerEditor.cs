﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CameraController)), CanEditMultipleObjects]
public sealed class CameraControllerEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.serializedObject.Update();
		this.CameraInspector();
		EditorGUILayout.Space();
		this.PitchInspector();
		EditorGUILayout.Space();
		this.YawInspector();
		EditorGUILayout.Space();
		this.DistanceInspector();
		EditorGUILayout.Space();
		this.ZoomInspector();
		base.serializedObject.ApplyModifiedProperties();
	}

	public void CameraInspector()
	{
		this.Property("camera");
		this.Property("heightOffset");
	}

	private void PitchInspector()
	{
		SerializedProperty pitchMinProperty = this.GetProperty("pitchMin");
		SerializedProperty pitchMaxProperty = this.GetProperty("pitchMax");

		float pitchMin = pitchMinProperty.floatValue;
		float pitchMax = pitchMaxProperty.floatValue;

		Range("Pitch Range", ref pitchMin, ref pitchMax);
		EditorGUILayout.MinMaxSlider(ref pitchMin, ref pitchMax, CameraController.MinPitch, CameraController.MaxPitch);

		pitchMinProperty.floatValue = pitchMin;
		pitchMaxProperty.floatValue = pitchMax;

		PropertySlider(this.GetProperty("pitchStart"), pitchMin, pitchMax);
	}

	private void YawInspector()
	{
		SerializedProperty clampYaw = this.GetProperty("clampYaw");
		EditorGUILayout.PropertyField(clampYaw);

		SerializedProperty yawMinProperty = this.GetProperty("yawMin");
		SerializedProperty yawMaxProperty = this.GetProperty("yawMax");

		float yawMin = yawMinProperty.floatValue;
		float yawMax = yawMaxProperty.floatValue;

		EditorGUI.BeginDisabledGroup(!clampYaw.boolValue);
		{
			Range("Yaw Range", ref yawMin, ref yawMax);
		}
		EditorGUI.EndDisabledGroup();

		yawMinProperty.floatValue = yawMin;
		yawMaxProperty.floatValue = yawMax;

		if (clampYaw.boolValue)
			PropertySlider(this.GetProperty("yawStart"), yawMin, yawMax);
		else
			this.Property("yawStart");
	}

	private void DistanceInspector()
	{
		SerializedProperty distanceMinProperty = this.GetProperty("distanceMin");
		SerializedProperty distanceMaxProperty = this.GetProperty("distanceMax");

		float distanceMin = distanceMinProperty.floatValue;
		float distanceMax = distanceMaxProperty.floatValue;

		Range("Distance Range", ref distanceMin, ref distanceMax);

		distanceMinProperty.floatValue = distanceMin;
		distanceMaxProperty.floatValue = distanceMax;

		PropertySlider(this.GetProperty("distanceStart"), distanceMin, distanceMax);
	}

	private void ZoomInspector()
	{
		SerializedProperty dampenZoom = this.GetProperty("dampenZoom");
		EditorGUILayout.PropertyField(dampenZoom);

		EditorGUI.BeginDisabledGroup(!dampenZoom.boolValue);
		{
			this.Property("zoomDampening");
		}
		EditorGUI.EndDisabledGroup();

		EditorGUILayout.Space();

		this.Property("obstructionLayers");
		this.Property("obstructionHitOffset");

		EditorGUI.BeginDisabledGroup(!dampenZoom.boolValue);
		{
			this.Property("obstructionZoomDampening");
		}
		EditorGUI.EndDisabledGroup();
	}

	private void Property(string name)
	{
		EditorGUILayout.PropertyField(this.GetProperty(name));
	}

	private SerializedProperty GetProperty(string name)
	{
		return base.serializedObject.FindProperty(name);
	}

	private static void Range(string label, ref float min, ref float max)
	{
		EditorGUILayout.BeginHorizontal();
		{
			float temp = EditorGUIUtility.labelWidth;

			EditorGUIUtility.labelWidth = 5;
			EditorGUILayout.LabelField(label);
			EditorGUIUtility.labelWidth = 28;
			min = EditorGUILayout.FloatField("Min", min);
			max = EditorGUILayout.FloatField("Max", max);

			EditorGUIUtility.labelWidth = temp;
		}
		EditorGUILayout.EndHorizontal();
	}

	private static void PropertySlider(SerializedProperty property, float min, float max)
	{
		property.floatValue = EditorGUILayout.Slider(property.displayName, property.floatValue, min, max);
	}
}
