﻿using UnityEngine;
using UnityEngine.Events;

using System.Collections.Generic;

[ExecuteInEditMode]
public sealed class CameraController : MonoBehaviour
{
	#region Static Reference
	public static CameraController ActiveController
	{
		get;
		private set;
	}

	public static void Disable()
	{
		if (ActiveController)
			ActiveController.enabled = false;
	}
	#endregion

	#region Constants
	public const float MinPitch = -90;
	public const float MaxPitch = 90;

	public const float MinDistance = 0;

	public const float MinDampening = 0;
	private const float DampeningModifier = 15;
	private const float DampeningEpsilon = 0.00002f;
	#endregion

	#region Inspector
	[SerializeField]
	private new Camera camera;
	[SerializeField]
	private float heightOffset = 0;

	[SerializeField]
	private float pitchMin = 15;
	[SerializeField]
	private float pitchMax = 55;
	[SerializeField]
	private float pitchStart = 30;

	[SerializeField]
	private bool clampYaw = false;
	[SerializeField]
	private float yawMin = 0;
	[SerializeField]
	private float yawMax = 0;
	[SerializeField]
	private float yawStart = 0;

	[SerializeField]
	private float distanceMin = 5;
	[SerializeField]
	private float distanceMax = 50;
	[SerializeField]
	private float distanceStart = 25;

	[SerializeField]
	private bool dampenZoom = false;
	[SerializeField]
	private float zoomDampening = 1.5f;

	// Thoughts about using the word "obstruction" in the field names
	[SerializeField]
	private LayerMask obstructionLayers = -1;
	[SerializeField]
	private float obstructionHitOffset = 0.25f;
	[SerializeField]
	private float obstructionZoomDampening = 1;


#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		// Currently Broken (sorta)
		if (this.IsActiveController)
		{
			Matrix4x4 tempMatrix = Gizmos.matrix;
			Color tempColor = Gizmos.color;

			Gizmos.color = Color.gray;

			Gizmos.matrix = Matrix4x4.TRS(
				this.cameraTransform.position,
				this.cameraTransform.rotation,
				new Vector3(1f, this.camera.aspect, 1f));

			Gizmos.DrawFrustum(
				Vector3.zero,
				this.camera.fieldOfView,
				this.camera.farClipPlane,
				this.camera.nearClipPlane,
				this.camera.aspect);

			Gizmos.DrawIcon(this.cameraTransform.position, "Camera", true);

			Gizmos.matrix = tempMatrix;
			Gizmos.color = tempColor;
		}
	}
#endif
	#endregion

	#region Validation
	void OnValidate()
	{
		this.ValidateCamera();
		this.ValidatePitchMin();
		this.ValidatePitchMax();
		this.ValidatePitchStart();
		this.ValidateYawMin();
		this.ValidateYawMax();
		this.ValidateYawStart();
		this.ValidateDistanceMin();
		this.ValidateDistanceMax();
		this.ValidateDistanceStart();
		this.ValidateZoomDampening();
		this.ValidateObstructionZoomDampening();
	}

	private void ValidateCamera()
	{
		if (!this.camera)
			base.enabled = false;
	}

	private void ValidatePitchMin()
	{
		this.pitchMin = Mathf.Clamp(this.pitchMin, MinPitch, this.pitchMax);
	}

	private void ValidatePitchMax()
	{
		this.pitchMax = Mathf.Clamp(this.pitchMax, this.pitchMin, MaxPitch);
	}

	private void ValidatePitchStart()
	{
		this.pitchStart = Mathf.Clamp(this.pitchStart, this.pitchMin, this.pitchMax);
	}

	private void ValidateYawMin()
	{
		this.yawMin = this.yawMin > this.yawMax ? this.yawMax : this.yawMin;
	}

	private void ValidateYawMax()
	{
		this.yawMax = this.yawMax < this.yawMin ? this.yawMin : this.yawMax;
	}

	private void ValidateYawStart()
	{
		this.yawStart = Mathf.Clamp(this.yawStart, this.yawMin, this.yawMax);
	}

	private void ValidateDistanceMin()
	{
		this.distanceMin = Mathf.Clamp(this.distanceMin, MinDistance, this.distanceMax);
	}

	private void ValidateDistanceMax()
	{
		this.distanceMax = this.distanceMax < this.distanceMin ? this.distanceMin : this.distanceMax;
	}

	private void ValidateDistanceStart()
	{
		this.distanceStart = Mathf.Clamp(this.distanceStart, this.distanceMin, this.distanceMax);
	}

	private void ValidateZoomDampening()
	{
		this.zoomDampening = this.zoomDampening < MinDampening ? MinDampening : this.zoomDampening;
	}

	private void ValidateObstructionZoomDampening()
	{
		this.obstructionZoomDampening = this.obstructionZoomDampening < MinDampening ? MinDampening : this.obstructionZoomDampening;
	}

	private void ValidatePitch()
	{
		this.pitch = Mathf.Clamp(this.pitch, this.pitchMin, this.pitchMax);
	}

	private void ValidateYaw()
	{
		this.yaw = Mathf.Clamp(this.yaw, this.yawMin, this.yawMax);
	}

	private void ValidateDistance()
	{
		this.distance = Mathf.Clamp(this.distance, this.distanceMin, this.distanceMax);
	}
	#endregion

	#region Fields
	private new Transform transform;
	private Transform cameraTransform;

	private float pitch, yaw, distance;
	private float distanceCorrected, distanceCurrent;
	#endregion

	#region Properties
	public bool IsActiveController
	{
		get
		{
			return this == ActiveController;
		}
	}

	public Camera Camera
	{
		get
		{
			return this.camera;
		}
		set
		{
			this.camera = value;
			this.ValidateCamera();
		}
	}

	public float HeightOffset
	{
		get
		{
			return this.heightOffset;
		}
		set
		{
			this.heightOffset = value;
		}
	}

	public float PitchMin
	{
		get
		{
			return this.pitchMin;
		}
		set
		{
			this.pitchMin = value;
			this.ValidatePitchMin();
			this.ValidatePitchStart();
#if UNITY_EDITOR
			if (Application.isPlaying)
			{
#endif
				this.ValidatePitch();
#if UNITY_EDITOR
			}
#endif
		}
	}

	public float PitchMax
	{
		get
		{
			return this.pitchMax;
		}
		set
		{
			this.pitchMax = value;
			this.ValidatePitchMax();
			this.ValidatePitchStart();
#if UNITY_EDITOR
			if (Application.isPlaying)
			{
#endif
				this.ValidatePitch();
#if UNITY_EDITOR
			}
#endif
		}
	}

	public float PitchStart
	{
		get
		{
			return this.pitchStart;
		}
		set
		{
			this.pitchStart = value;
			this.ValidatePitchStart();
		}
	}

	public bool ClampYaw
	{
		get
		{
			return this.clampYaw;
		}
		set
		{
			if (this.clampYaw = value)
			{
				this.ValidateYawStart();
#if UNITY_EDITOR
				if (Application.isPlaying)
				{
#endif
					this.ValidateYaw();
#if UNITY_EDITOR
				}
#endif
			}
		}
	}

	public float YawMin
	{
		get
		{
			return this.yawMin;
		}
		set
		{
			this.yawMin = value;
			this.ValidateYawMin();
			if (this.clampYaw)
			{
				this.ValidateYawStart();
#if UNITY_EDITOR
				if (Application.isPlaying)
				{
#endif
					this.ValidateYaw();
#if UNITY_EDITOR
				}
#endif
			}
		}
	}

	public float YawMax
	{
		get
		{
			return this.yawMax;
		}
		set
		{
			this.yawMax = value;
			this.ValidateYawMax();
			if (this.clampYaw)
			{
				this.ValidateYawStart();
#if UNITY_EDITOR
				if (Application.isPlaying)
				{
#endif
					this.ValidateYaw();
#if UNITY_EDITOR
				}
#endif
			}
		}
	}

	public float YawStart
	{
		get
		{
			return this.yawStart;
		}
		set
		{
			this.yawStart = value;
			this.ValidateYawStart();
		}
	}

	public float DistanceMin
	{
		get
		{
			return this.distanceMin;
		}
		set
		{
			this.distanceMin = value;
			this.ValidateDistanceMin();
			this.ValidateDistanceStart();
#if UNITY_EDITOR
			if (Application.isPlaying)
			{
#endif
				this.ValidateDistance();
#if UNITY_EDITOR
			}
#endif
		}
	}

	public float DistanceMax
	{
		get
		{
			return this.distanceMax;
		}
		set
		{
			this.distanceMax = value;
			this.ValidateDistanceMax();
			this.ValidateDistanceStart();
#if UNITY_EDITOR
			if (Application.isPlaying)
			{
#endif
				this.ValidateDistance();
#if UNITY_EDITOR
			}
#endif
		}
	}

	public float DistanceStart
	{
		get
		{
			return this.distanceStart;
		}
		set
		{
			this.distanceStart = value;
			this.ValidateDistanceStart();
		}
	}

	public bool DampenZoom
	{
		get
		{
			return this.dampenZoom;
		}
		set
		{
			this.dampenZoom = value;
		}
	}

	public float ZoomDampening
	{
		get
		{
			return this.zoomDampening;
		}
		set
		{
			this.zoomDampening = value;
			this.ValidateZoomDampening();
		}
	}

	public LayerMask ObstructionLayers
	{
		get
		{
			return this.obstructionLayers;
		}
		set
		{
			this.obstructionLayers = value;
		}
	}

	public float ObstructionHitOffset
	{
		get
		{
			return this.obstructionHitOffset;
		}
		set
		{
			this.obstructionHitOffset = value;
		}
	}

	public float ObstructionZoomDampening
	{
		get
		{
			return this.obstructionZoomDampening;
		}
		set
		{
			this.obstructionZoomDampening = value;
			this.ValidateObstructionZoomDampening();
		}
	}

	public Transform CameraTransform
	{
		get
		{
			return this.cameraTransform;
		}
	}

	public float Pitch
	{
		get
		{
			return this.pitch;
		}
		set
		{
			this.pitch = value;
			this.ValidatePitch();
		}
	}

	public float Yaw
	{
		get
		{
			return this.yaw;
		}
		set
		{
			this.yaw = value;
			if (this.clampYaw)
				this.ValidateYaw();
		}
	}

	public float Distance
	{
		get
		{
			return this.distance;
		}
		set
		{
			this.distance = value;
			this.ValidateDistance();
		}
	}

	public Vector3 Forward
	{
		get
		{
			return Quaternion.Euler(0f, this.yaw, 0f) * Vector3.forward;
		}
	}
	public Vector3 Back
	{
		get
		{
			return Quaternion.Euler(0f, this.yaw, 0f) * Vector3.back;
		}
	}
	public Vector3 Right
	{
		get
		{
			return Quaternion.Euler(0f, this.yaw, 0f) * Vector3.right;
		}
	}
	public Vector3 Left
	{
		get
		{
			return Quaternion.Euler(0f, this.yaw, 0f) * Vector3.left;
		}
	}

	public Quaternion ForwardRotation
	{
		get
		{
			return Quaternion.Euler(0f, this.yaw, 0f);
		}
	}
	public Quaternion BackRotation
	{
		get
		{
			return Quaternion.Euler(0f, this.yaw + 180, 0f);
		}
	}
	public Quaternion RightRotation
	{
		get
		{
			return Quaternion.Euler(0f, this.yaw + 90, 0f);
		}
	}
	public Quaternion LeftRotation
	{
		get
		{
			return Quaternion.Euler(0f, this.yaw + 270, 0f);
		}
	}
	#endregion

	#region Callback Methods
	void Awake()
	{
		this.transform = base.transform;
		this.ApplyStartValues();
	}

	void OnEnable()
	{
		if (this.camera)
		{
			Disable();
			this.cameraTransform = this.camera.transform;
			this.UpdateCamera(false);
			ActiveController = this;
		}
		else
		{
			base.enabled = false;
		}
	}

	void OnDisable()
	{
		if (this.IsActiveController)
		{
			ActiveController = null;
			this.cameraTransform = null;
		}
	}

	void LateUpdate()
	{
		if (this.camera)
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				this.ApplyStartValues();
			this.UpdateCamera(Application.isPlaying && this.dampenZoom);
#else
			this.UpdateCamera(this.dampenZoom);
#endif
		}
		else
		{
			base.enabled = false;
		}
	}
	#endregion

	#region Methods
	public void ApplyStartValues()
	{
		this.Pitch = this.pitchStart;
		this.Yaw = this.yawStart;
		this.Distance = this.distanceStart;
	}

	private void UpdateCamera(bool dampenZoom)
	{
		// Calculate target point that the camera will look at
		Vector3 targetPoint = this.transform.position;
		targetPoint.y += this.heightOffset;

		// Calculate ware the camera should be based on pitch yaw and distance
		Quaternion rotation = Quaternion.Euler(this.pitch, this.yaw, 0f);
		Vector3 position = targetPoint - (rotation * Vector3.forward * this.distance);

		// Prepare values for line cast and dampening
		RaycastHit hit;
		float dampeningValue;

		// Line cast from the target point to the camera to detect any obstructions
		if (Physics.Linecast(targetPoint, position, out hit, this.obstructionLayers))
		{
			this.distanceCorrected = Vector3.Distance(targetPoint, hit.point) - this.obstructionHitOffset;
			dampeningValue = this.CalcDampeningValue(this.obstructionZoomDampening);
		}
		else
		{
			this.distanceCorrected = this.distance;
			dampeningValue = this.CalcDampeningValue(this.zoomDampening);
		}

		// Determine dampening is required and perform a dampen lerp if so
		if (dampenZoom && Mathf.Abs(this.distanceCurrent - this.distanceCorrected) > DampeningEpsilon)
		{
			this.distanceCurrent = Mathf.Lerp(this.distanceCurrent, this.distanceCorrected, dampeningValue);
		}
		else
		{
			this.distanceCurrent = this.distanceCorrected;
		}

		// Recalculate position based on dampening results
		position = targetPoint - (rotation * Vector3.forward * this.distanceCurrent);

		// Set camera transform's position and rotation
		this.cameraTransform.position = position;
		this.cameraTransform.rotation = rotation;
	}

	private float CalcDampeningValue(float dampening)
	{
		return dampening > 0 ? DampeningModifier / dampening * Time.deltaTime : 1;
	}

	// The calc movement method that gets called by `PlayerMovement`
	public bool CalcMovement(float horizontal, float vertical, out Vector3 movement)
	{
		// Calcs raw movement
		bool result = this.CalcMovementRaw(horizontal, vertical, out movement);
		// If movement squared magnitude is greater than 1, normalize it
		if (movement.sqrMagnitude > 1)
			movement.Normalize();
		// return the result
		return result;
	}

	// Called by the CalcMovement method above
	public bool CalcMovementRaw(float horizontal, float vertical, out Vector3 movement)
	{
		// If both horizontal and verical inputs are 0 there is no movement
		if (horizontal != 0 || vertical != 0)
		{
			// Calculate the movement based on camera controller yaw and return true
			movement = Quaternion.Euler(0f, this.yaw, 0f) * new Vector3(horizontal, 0f, vertical);
			return true;
		}
		else
		{
			// Return a constant vector of 0,0,0 and return false
			movement = Vector3.zero;
			return false;
		}
	}

	public bool CalcRotation(float horizontal, float vertical, out Quaternion rotation)
	{
		if (horizontal != 0 || vertical != 0)
		{
			rotation = Quaternion.Euler(0f, this.yaw + Mathf.Atan2(horizontal, vertical) * Mathf.Rad2Deg, 0f);
			return true;
		}
		else
		{
			rotation = Quaternion.identity;
			return false;
		}
	}

	public bool CalcMovementAndRotation(float horizontal, float vertical, out Vector3 movement, out Quaternion rotation)
	{
		bool result = this.CalcMovementRawAndRotation(horizontal, vertical, out movement, out rotation);
		if (movement.sqrMagnitude > 1)
			movement.Normalize();
		return result;
	}

	public bool CalcMovementRawAndRotation(float horizontal, float vertical, out Vector3 movement, out Quaternion rotation)
	{
		if (horizontal != 0 || vertical != 0)
		{
			movement = Quaternion.Euler(0f, this.yaw, 0f) * new Vector3(horizontal, 0f, vertical);
			rotation = Quaternion.Euler(0f, this.yaw + Mathf.Atan2(horizontal, vertical) * Mathf.Rad2Deg, 0f);
			return true;
		}
		else
		{
			movement = Vector3.zero;
			rotation = Quaternion.identity;
			return false;
		}
	}
	#endregion
}
