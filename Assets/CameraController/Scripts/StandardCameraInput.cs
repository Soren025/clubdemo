﻿using UnityEngine;

[RequireComponent(typeof(CameraController))]
public class StandardCameraInput : MonoBehaviour
{
	public float pitchRotation = 750;
	public float yawRotation = 750;

	public float zoomSpeed = 1000;

#if !UNITY_ANDROID
	private CameraController cameraController;

	void Awake()
	{
		this.cameraController = base.GetComponent<CameraController>();
	}

	void Update()
	{
		this.cameraController.Pitch += -Input.GetAxis("Mouse Y") * this.pitchRotation * Time.deltaTime;
		this.cameraController.Yaw += Input.GetAxis("Mouse X") * this.yawRotation * Time.deltaTime;
		this.cameraController.Distance += -Input.GetAxis("Mouse ScrollWheel") * this.zoomSpeed * Time.deltaTime;
	}
#endif
}
