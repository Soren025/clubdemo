﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	// Inspector value to set the strength of the rotational force applied to the ball
	public float rollTorque;

	// Reference to other components
	private new Rigidbody rigidbody;
	private CameraController cameraController;

	// Called once on creation
	void Awake()
	{
		// Obtain other componenrs
		this.rigidbody = base.GetComponent<Rigidbody>();
		this.cameraController = base.GetComponent<CameraController>();

		// Some modifications to gravity and max angular velocity
		this.rigidbody.maxAngularVelocity *= 5;
		Physics.gravity *= 2;
	}

	// Called at a fixed rate before physics checks
	void FixedUpdate()
	{
		// Gets A/D input
		float horizontal = Input.GetAxis("Horizontal");
		// Gets W/S input
		float vertical = Input.GetAxis("Vertical");
		// Variable to hold our movement vector
		Vector3 movement;

		// Calc movement based on camera look direction. If no input was given will return false
		if (this.cameraController.CalcMovement(horizontal, vertical, out movement))
		{
			// Calculate rotational force based on direction to move
			Vector3 torque = new Vector3(movement.z, 0f, -movement.x) * this.rollTorque * Time.deltaTime;
			// Apply the rotational force to the rigidbody
			this.rigidbody.AddTorque(torque);
		}
	}
}
